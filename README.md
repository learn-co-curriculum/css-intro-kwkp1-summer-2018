# You will be able to

1. Explain the purpose and structure of **CSS**
2. **Link** an **HTML** file to a **CSS** file
3. Select specific content to style using tag **names**, **classes**, and **ids**
4. Center text using the **text-align** property
5. Modify content placement with **margin**, **padding**, and **border** properties
6. Change **image size** using height and width properties
7. Add **background colors** using the background property
8. Use hexadecimal, RGBA, and RBG **color values**
9. Change the **font** of text using font-size, font-family and color properties
10. Move sections of content around the page using the **float** property
11. Master reading **documentation** of CSS properties


# CSS

<img src="https://curriculum-content.s3.amazonaws.com/KWK/css-intro-awesome.jpg" alt="mug printed with the words CSS Is Awesome extending from a containing box" align="right" hspace="10" height="200" />

We learned all about HTML and how to mark up the content of our page, but now it's time to give those pages some style. CSS—**Cascading Stylesheets**—lets us change colors, positions, sizes and many other properties of the elements on our pages. The way we choose to style elements will make sure that users have a good experience with our website.
